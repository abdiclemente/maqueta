using Maqueta.Application.Extensions;
using Maqueta.Application.Interfaces;
using Maqueta.Application.Services;
using Maqueta.Domain.Entities;
using Maqueta.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IPersonaApplication, PersonaApplication>();

builder.Services.AddDbContext<CustomsoftContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("CustomsoftDB")));

//Application
var configuration = builder.Configuration;
builder.Services.RegisterApplicationProject();
//builder.Services.AddFluentValidationAutoValidation();
//builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());

//Infrastructure
builder.Services.RegisterInfrastructureProject();
//builder.Services.AddTransient<IPersonaRepository, PersonaRepository>();
//builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();





var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
