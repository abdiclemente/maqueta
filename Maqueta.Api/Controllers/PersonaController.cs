﻿using Maqueta.Application.Dtos.Request;
using Maqueta.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace Maqueta.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    { 

        private readonly IPersonaApplication _personaApplication;


        public PersonaController(IPersonaApplication personaAplication)
        {
            _personaApplication = personaAplication;

        }

        //CRUD a nivel API
        [HttpGet]
        public async Task<IActionResult> GetPersonas()
        {
            var response = await _personaApplication.GetPersonas();


            return Ok(response);
        }

        [HttpGet("{personaId:int}")]
        public async Task<IActionResult> GetPersona(int personaId)
        {
            var response = await _personaApplication.GetPersona(personaId);
            return Ok(response);
        }

        [HttpPost("{Register}")]
        public async Task<IActionResult> NewPersona(PersonaRequestDto requestDto)
        {
            var response = await _personaApplication.NewPersona(requestDto);
            return Ok(response);
        }

        [HttpPut("Edit/{personaId:int}")]
        public async Task<IActionResult> EditPersona(int personaId, PersonaRequestDto requestDto)
        {
            var response = await _personaApplication.EditPersona(personaId, requestDto);
            return Ok(response);
        }

        [HttpPut("Remove/{personaId:int}")]
        public async Task<IActionResult> RemovePersona(int personaId)
        {
            var response = await _personaApplication.RemovePersona(personaId);
            return Ok(response);
        }

    }
}
