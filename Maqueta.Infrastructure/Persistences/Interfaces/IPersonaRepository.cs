﻿using Maqueta.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Infrastructure.Persistences.Interfaces
{
    public interface IPersonaRepository
    {

        Task<IEnumerable<Persona>> GetPersonas();

        Task<Persona> GetPersona(int personaId);

        Task<bool> NewPersona(Persona persona);

        Task<bool> EditPersona(Persona persona);

        Task<bool> RemovePersona(int personaId);
    }
}
