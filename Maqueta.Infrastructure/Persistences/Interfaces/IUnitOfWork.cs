﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Infrastructure.Persistences.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        //Declaracion de nuestra interface a nivel de repository
        IPersonaRepository Persona { get; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
