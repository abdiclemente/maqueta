﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Maqueta.Domain.Entities;

public partial class CustomsoftContext : DbContext
{
    public CustomsoftContext()
    {
    }

    public CustomsoftContext(DbContextOptions<CustomsoftContext> options)
        : base(options)
    {
    }
    

    public virtual DbSet<Persona> Personas { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Persona>(entity =>
        {
            entity.HasKey(e => e.PersonaId).HasName("persona_pkey");

            entity.ToTable("persona");

            entity.Property(e => e.PersonaId).HasColumnName("persona_id");
            entity.Property(e => e.ApellidoMaterno)
                .HasMaxLength(100)
                .HasColumnName("apellido_materno");
            entity.Property(e => e.ApellidoPaterno)
                .HasMaxLength(100)
                .HasColumnName("apellido_paterno");
            entity.Property(e => e.AuditCreateDate)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("audit_create_date");
            entity.Property(e => e.AuditCreateUser).HasColumnName("audit_create_user");
            entity.Property(e => e.AuditDeleteDate)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("audit_delete_date");
            entity.Property(e => e.AuditDeleteUser).HasColumnName("audit_delete_user");
            entity.Property(e => e.AuditUpdateDate)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("audit_update_date");
            entity.Property(e => e.AuditUpdateUser).HasColumnName("audit_update_user");
            entity.Property(e => e.ClaveCia)
                .HasMaxLength(100)
                .HasColumnName("clave_cia");
            entity.Property(e => e.Curp)
                .HasMaxLength(100)
                .HasColumnName("curp");
            entity.Property(e => e.FechaNacimiento)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("fecha_nacimiento");
            entity.Property(e => e.Nombre)
                .HasMaxLength(100)
                .HasColumnName("nombre");
            entity.Property(e => e.Rfc)
                .HasMaxLength(100)
                .HasColumnName("rfc");
            entity.Property(e => e.SexoId).HasColumnName("sexo_id");
            entity.Property(e => e.Snombre)
                .HasMaxLength(100)
                .HasColumnName("snombre");
            entity.Property(e => e.State).HasColumnName("state");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
