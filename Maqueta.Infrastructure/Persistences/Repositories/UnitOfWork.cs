﻿using Maqueta.Domain.Entities;
using Maqueta.Infrastructure.Persistences.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Infrastructure.Persistences.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CustomsoftContext _context;

        public IPersonaRepository Persona { get; private set; }

        public UnitOfWork(CustomsoftContext context)
        {
            _context = context;
            Persona = new PersonaRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void SaveChanges()
        {
            _context.SaveChangesAsync();
        }

        public Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }
    }
}
