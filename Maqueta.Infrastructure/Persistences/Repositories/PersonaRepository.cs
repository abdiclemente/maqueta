﻿using Maqueta.Domain.Entities;
using Maqueta.Infrastructure.Commons;
using Maqueta.Infrastructure.Commons.Bases;
using Maqueta.Infrastructure.Persistences.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Infrastructure.Persistences.Repositories
{
    public class PersonaRepository : IPersonaRepository
    {
        private readonly CustomsoftContext _context;
        public PersonaRepository(CustomsoftContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Persona>> GetPersonas()
        {
            var personas = await _context.Personas
               .Where(x => x.State.Equals(1) && x.AuditDeleteUser == null && x.AuditDeleteDate == null).AsNoTracking().ToListAsync();
            return personas;
        }

        public async Task<Persona> GetPersona(int personaId)
        {
            var personas = await _context.Personas.AsNoTracking().FirstOrDefaultAsync(x => x.PersonaId.Equals(personaId));
            return personas!;
        }

        public async Task<bool> NewPersona(Persona persona)
        {
            persona.AuditCreateUser = 1;
            persona.AuditCreateDate = DateTime.Now;

            await _context.AddAsync(persona);

            var recordsAffected = await _context.SaveChangesAsync();
            return recordsAffected > 0;
        }

        public async Task<bool> EditPersona(Persona persona)
        {
            persona.AuditCreateUser = 1;
            persona.AuditCreateDate = DateTime.Now;
            _context.Update(persona);
            _context.Entry(persona).Property(x => x.AuditCreateUser).IsModified = false;
            _context.Entry(persona).Property(x => x.AuditCreateDate).IsModified = false;

            var recordsAffected = await _context.SaveChangesAsync();
            return recordsAffected > 0;
        }

        public async Task<bool> RemovePersona(int personaId)
        {
            var persona = await _context.Personas.AsNoTracking().SingleOrDefaultAsync(x => x.PersonaId.Equals(personaId));

            persona!.AuditDeleteUser = 1;
            persona.AuditDeleteDate = DateTime.Now;

            _context.Update(persona);
            _context.Entry(persona).Property(x => x.AuditCreateUser).IsModified = false;
            _context.Entry(persona).Property(x => x.AuditCreateDate).IsModified = false;

            var recordsAffected = await _context.SaveChangesAsync();
            return recordsAffected > 0;
        }
    }
}
