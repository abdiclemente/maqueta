﻿using Maqueta.Infrastructure.Persistences.Interfaces;
using Maqueta.Infrastructure.Persistences.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Infrastructure.Extensions
{
    public static class RegisterInfrastructureProjectModule
    {
        public static IServiceCollection RegisterInfrastructureProject(this IServiceCollection services)
        {
            // registrar los servicios de infrastructura
            services.AddTransient<IPersonaRepository, PersonaRepository>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
