﻿using FluentValidation.AspNetCore;
using Maqueta.Application.Interfaces;
using Maqueta.Application.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Maqueta.Application.Extensions
{
    public static class RegisterApplicationProjectModule
    {
        public static IServiceCollection RegisterApplicationProject(this IServiceCollection services)
        {
            services.AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies().Where(p => !p.IsDynamic));
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<IPersonaApplication, PersonaApplication>();

            return services;
        }
    }
}
