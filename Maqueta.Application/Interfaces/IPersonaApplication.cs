﻿using Maqueta.Application.Commons.Bases;
using Maqueta.Application.Dtos.Request;
using Maqueta.Application.Dtos.Response;
using Maqueta.Infrastructure.Commons.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Interfaces
{
    public interface IPersonaApplication
    {
        //CRUD a nivel aplicacion
        Task<BaseResponse<IEnumerable<PersonaResponseDto>>> GetPersonas();

        Task<BaseResponse<PersonaResponseDto>> GetPersona(int personaId);

        Task<BaseResponse<bool>> NewPersona(PersonaRequestDto requestDto);

        Task<BaseResponse<bool>> EditPersona(int personaId, PersonaRequestDto requestDto);

        Task<BaseResponse<bool>> RemovePersona(int personaId);
    }
}
