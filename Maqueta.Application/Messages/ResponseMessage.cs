﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Messages
{
    public class ResponseMessage
    {
        public const string MESSAGE_QUEY = "Consulta exitosa";
        public const string MESSAGE_QUEY_EMPTY = "No se encontraron registros";
        public const string MESSAGE_SAVE = "Se registró correctamente";
        public const string MESSAGE_UPDATE = "Se actualizó correctamente";
        public const string MESSAGE_DELETE = "Se eliminó correctamente";
        public const string MESSAGE_EXISTS = "El registro ya existe";
        public const string MESSAGE_ACTIVATE = "El registro ha sido activado";
        public const string MESSAGE_TOKEN = "Token generado correctamente";
        public const string MESSAGE_VALIDAYE = "Errores de validacion";
        public const string MESSAGE_FAILED = "Operacion fallida";
    }
}
