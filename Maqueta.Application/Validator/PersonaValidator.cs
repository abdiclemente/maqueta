﻿using FluentValidation;
using Maqueta.Application.Dtos.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Validator
{
    public class PersonaValidator : AbstractValidator<PersonaRequestDto>
    {
        public PersonaValidator()
        {
            RuleFor(x => x.Nombre)
                .NotNull().WithMessage("El campo nombre no puede ser nulo prueba")
                .NotEmpty().WithMessage("El campo nombre no puede ser vacio prueba");
        }
    }
}
