﻿using AutoMapper;
using Maqueta.Application.Commons.Bases;
using Maqueta.Application.Dtos.Request;
using Maqueta.Application.Dtos.Response;
using Maqueta.Application.Interfaces;
using Maqueta.Application.Messages;
using Maqueta.Application.Validator;
using Maqueta.Domain.Entities;
using Maqueta.Infrastructure.Commons.Bases;
using Maqueta.Infrastructure.Persistences.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Services
{
    public class PersonaApplication : IPersonaApplication
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly PersonaValidator _validationRules;

        public PersonaApplication(IUnitOfWork unitOfWork, IMapper mapper, PersonaValidator validationRules)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _validationRules = validationRules;
        }
        //CRUD a nivel aplicacion para interactuar con api
        public async Task<BaseResponse<IEnumerable<PersonaResponseDto>>> GetPersonas()
        {
            var response = new BaseResponse<IEnumerable<PersonaResponseDto>>();
            var personas = await _unitOfWork.Persona.GetPersonas();

            if (personas is not null)
            {
                response.IsSuccess = true;
                response.Data = _mapper.Map<IEnumerable<PersonaResponseDto>>(personas);
                response.Message = ResponseMessage.MESSAGE_QUEY;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_QUEY_EMPTY;
            }

            return response;
        }

        public async Task<BaseResponse<PersonaResponseDto>> GetPersona(int personaId)
        {
            var response = new BaseResponse<PersonaResponseDto>();
            var persona = await _unitOfWork.Persona.GetPersona(personaId);

            if (persona is not null)
            {
                response.IsSuccess = true;
                response.Data = _mapper.Map<PersonaResponseDto>(persona);
                response.Message = ResponseMessage.MESSAGE_QUEY;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_QUEY_EMPTY;
            }

            return response;
        }

        public async Task<BaseResponse<bool>> NewPersona(PersonaRequestDto requestDto)
        {
            var response = new BaseResponse<bool>();
            var validationResult = await _validationRules.ValidateAsync(requestDto);

            if (!validationResult.IsValid)
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_VALIDAYE;
                response.Errors = validationResult.Errors;
                return response;
            }
            var persona = _mapper.Map<Persona>(requestDto);
            response.Data = await _unitOfWork.Persona.NewPersona(persona);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = ResponseMessage.MESSAGE_SAVE;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_FAILED;
            }

            return response;
        }

        public async Task<BaseResponse<bool>> EditPersona(int personaId, PersonaRequestDto requestDto)
        {
            var response = new BaseResponse<bool>();
            var personaEdit = await GetPersona(personaId);

            if (personaEdit.Data is null)
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_UPDATE;
            }
            var persona = _mapper.Map<Persona>(requestDto);
            persona.PersonaId = personaId;
            response.Data = await _unitOfWork.Persona.EditPersona(persona);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = ResponseMessage.MESSAGE_UPDATE;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_FAILED;
            }
            return response;
        }

        public async Task<BaseResponse<bool>> RemovePersona(int personaId)
        {
            var response = new BaseResponse<bool>();
            var persona = await GetPersona(personaId);

            if (persona.Data is null)
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_QUEY_EMPTY;
            }

            response.Data = await _unitOfWork.Persona.RemovePersona(personaId);

            if (response.Data)
            {
                response.IsSuccess = true;
                response.Message = ResponseMessage.MESSAGE_DELETE;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = ResponseMessage.MESSAGE_FAILED;
            }

            return response;
        }
    }
}
