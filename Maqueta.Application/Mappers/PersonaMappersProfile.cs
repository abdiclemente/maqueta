﻿using AutoMapper;
using Maqueta.Application.Commons.Bases;
using Maqueta.Application.Dtos.Request;
using Maqueta.Application.Dtos.Response;
using Maqueta.Domain.Entities;
using Maqueta.Infrastructure.Commons.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Maqueta.Application.Mappers
{
    internal class PersonaMappersProfile : Profile
    {
        public PersonaMappersProfile()
        {

            CreateMap<PersonaRequestDto, Persona>();

            CreateMap<Persona, PersonaResponseDto>()
                .ReverseMap();

        }
        
    }
}
