﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Dtos.Response
{
    public class PersonaResponseDto
    {
        public int PersonaId { get; set; }
        public string Nombre { get; set; }
        public string? Snombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string? ApellidoMaterno { get; set; }
    }
}
