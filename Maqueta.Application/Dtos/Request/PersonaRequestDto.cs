﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maqueta.Application.Dtos.Request
{
    public class PersonaRequestDto
    {
        //public int PersonaId { get; set; }

        public string Nombre { get; set; } = null!;

        public string? Snombre { get; set; }

        public string ApellidoPaterno { get; set; } = null!;

        public string? ApellidoMaterno { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public int? State { get; set; }
    }
}
